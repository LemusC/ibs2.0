package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Venta;

public interface IVentaRepository extends CrudRepository<Venta, Long>{

}
