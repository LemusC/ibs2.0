package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Cliente;

public interface IClienteRepository extends CrudRepository<Cliente, Long>{


}
