package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Usuario;

public interface IUsuarioRepository extends CrudRepository<Usuario, Long>{
	
	Usuario findByUser(String user);
}
