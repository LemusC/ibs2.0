package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Ventasproducto;

public interface IVentaProductoRepository extends CrudRepository<Ventasproducto, Long>{

}
