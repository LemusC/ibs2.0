package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Proveedore;

public interface IProveedorRepository extends CrudRepository<Proveedore, Long>{

}
