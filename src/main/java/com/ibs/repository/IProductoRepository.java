package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Producto;

public interface IProductoRepository extends CrudRepository<Producto, Long>{

}
