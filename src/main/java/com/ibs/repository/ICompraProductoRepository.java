package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Comprasproducto;

public interface ICompraProductoRepository extends CrudRepository<Comprasproducto, Long>{

}
