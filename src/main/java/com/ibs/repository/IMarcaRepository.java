package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Marca;

public interface IMarcaRepository extends CrudRepository<Marca, Long>{

}
