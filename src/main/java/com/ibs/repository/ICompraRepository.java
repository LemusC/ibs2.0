package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Compra;

public interface ICompraRepository extends CrudRepository<Compra, Long>{

}
