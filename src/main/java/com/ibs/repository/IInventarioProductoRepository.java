package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Inventariosproducto;

public interface IInventarioProductoRepository extends CrudRepository<Inventariosproducto, Long>{

}
