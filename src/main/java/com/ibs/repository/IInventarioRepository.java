package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Inventario;

public interface IInventarioRepository extends CrudRepository<Inventario, Long>{

}
