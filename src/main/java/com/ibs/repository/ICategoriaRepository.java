package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibs.model.Categoria;

public interface ICategoriaRepository extends CrudRepository<Categoria, Long>{

}
